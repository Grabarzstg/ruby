### README ###

----- Cel pracy -----
Rozwiązywanie równania x^5 + x +7 na dwa sposoby: metodą siecznych i metodą połowienia. 
Porównanie ilości kroków w każdej z tych metod do uzyskania danej z wejścia dokładności 0 < a < 1

----- Jak uruchomić? ------
Poleceniem "rspec -f d" w głównym folderze.

Przykładowe dane wejściowe dla programu:

epsilon: 0.1
a: -1
b: -2