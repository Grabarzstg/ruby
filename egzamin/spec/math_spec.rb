require 'simplecov'
require 'rspec/expectations'
SimpleCov.start
require_relative '../lib/math.rb'

  def with_stdin
    stdin = $stdin             # remember $stdin
    $stdin, write = IO.pipe    # create pipe assigning its "read end" to $stdin
    yield write                # pass pipe's "write end" to block
  ensure
    write.close                # close pipe
    $stdin = stdin             # restore $stdin
  end

describe 'variables_class' do
  tempObj = Variables.new(1, 1, 1, 1, 1)
 
  it 'check if not raise error' do
	expect{Variables.new(1, 1, 1, 1, 1)}.to_not raise_error
  end
 
  it 'check if getEpsilon() works properly (good input)' do
	with_stdin do |user|
      user.puts 0.1
	  expect { tempObj.getEpsilon() }.to output('Podaj epsilon: (liczba z przedzialu {0..1})
').to_stdout
	  expect(tempObj.epsilon).to be_between(0, 1).exclusive
	end
  end
  
    it 'check if getRange() works properly' do
	with_stdin do |user|
      user.puts 2
	  user.puts 2
	  expect { tempObj.getRange() }.to change(tempObj, :x1).from(1).to(2)
	  expect(tempObj.x2).to eq(2)
	end
  end
  
  it 'check if setFunc() works properly' do
    tempObj.fa = 0
    expect { tempObj.setFunc(-1,-1) }.to change(tempObj, :fa).from(0).to(5)
  end
  
  it 'check if f(x) returns good values' do
    expect(tempObj.f(-1)).to eq(5)
  end
end

describe 'Bisection_class' do
  tempObj = Variables.new(-1.0, -2.0, 0.0, -1.0, -2.0)
  tempObj.setFunc(-1,-2)
  bisObj = Bisection.new
  
  it 'check if not raise error' do
	expect{Bisection.new}.to_not raise_error
  end
  
  it 'begining value in range' do
    expect(bisObj.instance_variable_get(:@steps)).to eql(0)
	expect(bisObj.instance_variable_get(:@result)).to eql(0)
  end
  
  it 'correct transition to the next method' do
  tempObj.epsilon=0.1
    bisObj.check(tempObj)
	expect(bisObj.instance_variable_get(:@steps)).to be > 0
  end
  
  it 'function run work as intended ' do
    expect(bisObj.instance_variable_get(:@result)).to eql(tempObj.x0)
  end
end

describe 'Slashing_class' do
  tempObj = Variables.new(-1.0, -2.0, 0.0, -1.0, -2.0)
  tempObj.epsilon = 0.1
  tempObj.setFunc(-1.0, -2.0)
  sls = Slashing.new
  
  it 'check if not raise error' do
	expect{Slashing.new}.to_not raise_error
  end
  
  it 'check if run() returns good values' do
    sls.run(tempObj)
    expect(sls.result).to be_between(-1.39, -1.389).inclusive
  end
  
  it 'check if run() do not generate errors' do
    expect{sls.run(tempObj)}.to_not raise_error
  end
end


