class Variables
  attr_accessor :a,:b,:x0,:x1,:x2,:fa,:fb,:f0,:epsilon
  
  def initialize(a, b, x0, x1, x2)
    @a = a
    @b = b
    @x0 = x0
    @x1 = x1
    @x2 = x2
  end

  def getEpsilon ()
    puts 'Podaj epsilon: (liczba z przedzialu {0..1})'
    epsilon = $stdin.gets.chomp.to_f
    if (epsilon >= 1 || epsilon <= 0)
      puts 'epsilon nalezy do przedzialu {0..1} !'
      getEpsilon();
    end
  @epsilon = epsilon
  end
  
  def getRange()
    puts 'Podaj zakres poszukiwan pierwiastka: Podaj a:'
    @a = $stdin.gets.chomp.to_f
    @x1 = @a
    puts 'Podaj b:'
    @b = $stdin.gets.chomp.to_f
    @x2 = @b
    setFunc(@a,@b)
  end

  def setFunc (a, b)
    @fa = f(a)
    @fb = f(b)
  end

  def f(x)
    return x * x * x * x * x + x + 7 
  end
end

class Bisection
  attr_accessor :steps,:result
  def initialize
    @steps = 0
	@result = 0
  end

  def check(init)
    while ((init.fa * init.fb) > 0)
	  puts 'Funkcja nie spelnia zalozen bisekcji!'
	  init.getRange()
	end
	run(init)
    
  end
  
  def run(init)
  while (init.a - init.b).abs > init.epsilon
    @steps += 1
    init.x0 = ((init.a + init.b) /2)
	init.f0 = init.f(init.x0)
	
	if init.f0.abs < init.epsilon
	  break
	end
	
	if (init.fa * init.f0) < 0
	  init.b = init.x0
	  else
	    init.a = init.x0
		init.fa = init.f0
	end
  end 
  @result = init.x0  
  end
end

class Slashing
  attr_accessor :steps,:result
  def initialize
    @steps = 0
	@result = 0
  end
  
  def run(init)
    i = 64
    init.setFunc(init.x1, init.x2)
    while  ((i > 0) && ((init.x1 - init.x2).abs > init.epsilon))
      @steps = @steps + 1
	  if ((init.fa - init.fb).abs) < init.epsilon
	    raise 'Zle punkty startowe'
	    i = 0
	  break
	  end
	
	  init.x0 = (init.x1 - init.fa * (init.x1 - init.x2) / (init.fa - init.fb))
	  init.f0 = init.f(init.x0)
	
	  if init.f0.abs < init.epsilon
	    break
	  end
	
	  init.x2 = init.x1
	  init.fb = init.fa
	  init.x1 = init.x0
	  init.fa = init.f0
	  i -= 1
	
	  if i<=0
	    raise 'Blad: Przekroczony limit obiegow.'
	  end
    end
    if i > 0
      @result = init.x0
    end
  end
  
end

def compare(bResult, sResult, bSteps, sSteps  )
  if bSteps != 0 && sSteps != 0
  puts
    puts '---------------------------------------------------------'
    puts '[BISEKCJA] x0 = ' + bResult.to_s
	puts '[SIECZNE ] x0 = ' + sResult.to_s
	puts '---------------------------------------------------------'
	puts '[BISEKCJA] ilosc krokow: ' + bSteps.to_s
	puts '[SIECZNE ] ilosc krokow: ' + sSteps.to_s
	puts '---------------------------------------------------------'
  end
end

bisData = Variables.new(1, 1, 1, 1, 1)
bisData.getEpsilon
bisData.getRange
slsData = bisData

bis = Bisection.new
bis.check(bisData)

sls = Slashing.new
sls.run(slsData)

compare(bis.result, sls.result, bis.steps, sls.steps)

