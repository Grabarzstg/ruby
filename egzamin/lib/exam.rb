def getEpsilon ()
puts "Podaj epsilon: (liczba z przedziału {0..1})"
$epsilon = $stdin.gets.chomp.to_f
  if ($epsilon > 1 || $epsilon < 0)
    puts "epsilon nalezy do przedziału {0..1} !  "
    getEpsilon();
  end
  getRange();
end

def f(x)
  return x * x * x * x * x + x + 7 
end

def getRange()
  puts "Podaj zakres poszukiwań pierwiastka: \n Podaj a:"
  $a = $stdin.gets.chomp.to_f
  $x1 = $a
  puts " Podaj b:"
  $b = $stdin.gets.chomp.to_f
  $x2 = $b
  setFunc($a, $b)
  checkBisection();
end

def setFunc (a, b)
  $fa = f(a)
  $fb = f(b)
end


def checkBisection()
  if (($fa * $fb) > 0)
	puts "Funkcja nie spełnia założeń bisekcji!"
	getRange()
	else
	  bisection()
  end
end

def bisection
  $bisectionSteps = 0
  while ($a - $b).abs > $epsilon
    $bisectionSteps += 1
    $x0 = (($a + $b) /2)
	$f0 = f($x0)
	
	if $f0.abs < $epsilon
	  break
	end
	
	if ($fa * $f0) < 0
	  $b = $x0
	  else
	    $a = $x0
		$fa = $f0
	end
  end 
  $bOut = $x0  
end

def slashing()
  $slashingSteps = 0
  $x0 = 0
  $f0 = 0
  $i = 64
  setFunc($x1, $x2)
  
  while  (($i > 0) && (($x1 - $x2).abs > $epsilon))
    $slashingSteps = $slashingSteps + 1
	if (($fa - $fb).abs) < $epsilon
	  puts "Złe punkty startowe"
	  $i = 0
	  break
	end
	
	$x0 = ($x1 - $fa * ($x1 - $x2) / ($fa - $fb))
	$f0 = f($x0)
	
	if $f0.abs < $epsilon
	  break
	end
	
	$x2 = $x1
	$fb = $fa
	$x1 = $x0
	$fa = $f0
	$i -= 1
	
	if $i<=0
	  puts "Błąd: Przekroczony limit obiegów."
	end
	
  end
  if $i > 0
    $sOut = $x0
  end
  
end

def compare()
  if $bisectionSteps != 0 && $slashingSteps != 0
    puts "[BISEKCJA] x0 = " + $bOut.to_s
	puts "[SIECZNE ] x0 = " + $sOut.to_s
	puts "[BISEKCJA] ilość kroków: " + $bisectionSteps.to_s
	puts "[SIECZNE ] ilośc kroków: " + $slashingSteps.to_s
  end
end
	
getEpsilon()
slashing()
compare()