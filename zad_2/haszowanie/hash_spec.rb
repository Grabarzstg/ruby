require 'simplecov'
SimpleCov.start
require_relative 'hash.rb'

describe "haszowanie" do
let(:t) {[nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]}
let(:out) {[0,0,1,nil,4,5,nil,nil,nil,nil]}

  it "check if not raise error" do
	expect{haszowanie(t,0)}.to_not raise_error
  end
  it "check if hash table works correctly" do

	haszowanie(t,0)
	haszowanie(t,0)
	haszowanie(t,5)
	haszowanie(t,1)
	haszowanie(t,4)
      (0..9).each do |counter|
  expect(t[counter]).to eq(out[counter])
  end
  end
end