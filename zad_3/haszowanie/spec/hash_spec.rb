require 'simplecov'
SimpleCov.start
require_relative '../lib/hash.rb'


describe "haszowanie" do
  context '#tables' do
    subject { [] }
    context 'without nothing inside' do
      it 'check if not raise error' do
        expect{haszowanie(subject,0)}.to_not raise_error
      end
      it 'check if initial table is empty' do
        expect(subject).to eq([])      
      end
    end
    
    context 'with numbers' do
      let(:out) {[0,0,1,nil,4,5]}
      it 'check if hash table works correctly' do
        haszowanie(subject,0)
        haszowanie(subject,0)
        haszowanie(subject,1)
        haszowanie(subject,4)
        haszowanie(subject,5)
        expect(subject).to eq out
      end
    end
  end 
end

