require 'simplecov'
SimpleCov.start
require_relative '../lib/heap.rb'

describe "parent" do
  it "check if not raise error" do
	expect{parent(2)}.to_not raise_error
  end
  it "parent returns i/2" do
    expect(parent(2)).to eq(1)
  end
end

describe "left" do
  it "check if not raise error" do
	expect{left(2)}.to_not raise_error
  end
  it "left returns 2*i" do
    expect(left(2)).to eq(4)
  end
end

describe "right" do
  it "check if not raise error" do
	expect{right(2)}.to_not raise_error
  end
  it "right returns 2*i+1" do
    expect(right(2)).to eq(5)
  end
end

describe "heapsort" do
let(:a) {[10,8,6,4,2,9,7,5,3,1]}

  it "check if not raise error" do
	expect{heapsort(a)}.to_not raise_error
  end
  
  it "test if element is lower than next one in the table " do
  heapsort(a)
  (0..8).each do |counter|
  expect(a[counter]).to be < a[counter+1] 
	end
  end 
end

describe "heapify" do

let(:input) {[3,1,2,4,5,6,7,8,9,10]}
let(:output)  {[3,2,1,4,5,6,7,8,9,10]}
  
  it "check if not raise error" do
	expect{heapify(input,1,3)}.to_not raise_error
  end

  it "test if heapify transform input into correct output " do
  heapify input,1,3
  (0..9).each do |counter|
  expect(input[counter]).to eq(output[counter])
    end
  end
end